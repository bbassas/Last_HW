public abstract class GameCharacter
{
    protected String name;
    protected int health;
    protected int attackPower;
    protected boolean isFighting;
    
    public GameCharacter(){
        name ="";
        health =0;
        attackPower=0;
        isFighting = false;
    }

    public GameCharacter(String name, int health, int attackPower)
    {
        this.name = name;
        this.health = health;
        this.attackPower = attackPower;
    }
    public int getHealth()
    {
        return this.health;
    }
    public String getName()
    {
        return this.name;
    }
    public int attackPower()
    {
        return this.attackPower;
    }
    public boolean isFighting()
    {
        return this.isFighting;
    }
    public void setHealth(int health)
    {
        this.health = health;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public void setAttackPower(int attackPower)
    {
        this.attackPower = attackPower;
    }
    public void setIsFighting(boolean isFighting)
    {
        this.isFighting = isFighting;
    }
    
    public void attack(GameCharacter fighting)
    {
        fighting.setHealth(fighting.getHealth() - this.attackPower);
    }
    public boolean isAlive()
    {
        if(this.health > 0)
        {
            return true;
        }
        else 
        {
            return false;
        }
        
    }
    
    public String toString()
    {
        return String.format("You are fighting %s%nHealth: %d%nAttackPower: %d", this.name, this.health, this.attackPower);
    }
    
   public abstract void takeTurn(GameCharacter opponent);
  
    
}