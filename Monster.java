import java.util.Random; 
public class Monster extends GameCharacter
{
    private Random randomMonster = new Random();
    public final int SUNNY = 0;
    public final int JOE = 1;
    public final int PAINE = 2;
    private Weapon mainItem;
    
    
    public Monster()
    {
        super();
        generateMonster();
    }
    
     public void createSunny()
    {
        super.name = "Sunny, the manic depressive.";
        super.attackPower = 4 + randomMonster.nextInt(2);
        super.health = 50 + randomMonster.nextInt(20);
        super.isFighting = true;
        mainItem = null;
    }
    
     public void createJoe()
   {
        super.name = "Joe, the ICP fanboy.";
        super.attackPower = 10 + randomMonster.nextInt(4);
        super.health = 65 + randomMonster.nextInt(20);
        super.isFighting = true;
         mainItem = null;
   }
    
     public void createPaine()
    {
        super.name = "Paine, the blackout drunk.";
        super.attackPower = 20 + randomMonster.nextInt(2);
        super.health = 75 + randomMonster.nextInt(20);
        super.isFighting = true;
         mainItem = new Weapon("Broken Bottle");
         if(super.health < 0 )
         {
             System.out.printf("You picked up a %s and killed the monster.",mainItem);
         }
    }
    
    public void generateMonster()
    {
        int randomNumber = randomMonster.nextInt(3);
        
        if (randomNumber == SUNNY)
        {
            createSunny();
        }
        else if (randomNumber == JOE)
        {
            createJoe();
        }
        else if (randomNumber == PAINE)
        {
            createPaine();
        }
    }
    public Weapon dropWeapon()
    {
        return this.mainItem;
    }
      
    public void takeTurn(GameCharacter player)
    {
        super.attack(player);
    }
}