
import java.util.Scanner;
import java.util.ArrayList;

public class Player extends GameCharacter
{
    
    private int playerMana;
    private int playerLevel;
    ArrayList<Potion> potions = new ArrayList<Potion>();
    ArrayList<Weapon> weapons = new ArrayList<Weapon>();
    private static int userInput;
    static Scanner input = new Scanner(System.in);
    
    public Player(String playerName, int playerHealth, int playerAttackPower, int playerMana, int playerLevel)
    {
        super(playerName, playerHealth, playerAttackPower);
        this.playerLevel = playerLevel;
        this.playerMana = playerMana;
    }

    
    public int getMana()
    {
        return playerMana;
    }
    public int getLevel()
    {
        return playerLevel;
    }
    public void setMana(int mana)
    {
        this.playerMana = mana;
    }
    public void setLevel(int level)
    {
        this.playerLevel = level;
    }
    
    public void takeTurn(GameCharacter monster)
    {
        System.out.println("1) cast a glare 2) punch");
        userInput=input.nextInt();
        if(userInput == 1)
        {
        monster.health -= 20;
        }
        else if (userInput == 2)
        {
            super.attack(monster);
        }
        else if(monster.health < 0)
        {
            System.out.println("You beat the monster.");
        }
       
    }
    public void pickWeapon(Weapon weapon)
    {
        weapons.add(weapon);
    }
      public void pickPotion(Potion potion)
    {
        potions.add(potion);
    }
    public void usePotion()
    {
        if(checkPotion() == true )
        {
            super.health += super.health / 2;
            potions.remove(0);
        }
    }
    public boolean checkPotion()
    {
        if(potions.size() >0 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
      public boolean checkWeapon()
    {
        if(weapons.size() >0 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public boolean checkInventory()
    {
        if(weapons.size() > 0 || potions.size() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public String getInventory()
    {
        String inventory = "";
        if (potions.size() > 0)
        {
            inventory += "You have this many potions:\n";
            for(int i = 0; potions.size() > i; i++)
            {
                inventory += potions.get(i).toString() + " ";
            }
        }
        
        if (weapons.size() > 0)
        {
            inventory += "You have the following weapons:\n";
            for(int i = 0; weapons.size() > i; i++)
            {
                inventory += weapons.get(i).toString() + " ";
            }
        }
        return inventory;
    }
    
}