public abstract class Item
{
    protected String mainItem;
    
    public Item(String giveItem)
    {
        this.mainItem = giveItem;
    }
    
    public String toString()
    {
        if(mainItem == null)
        {
            return "No item";
        }
        else
        {
            return mainItem;
        }
    }
}